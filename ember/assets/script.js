Ember.LOG_BINDINGS = true;

window.App = Ember.Application.create({
  LOG_TRANSITIONS: true,
  LOG_ACTIVE_GENERATION: true,
  LOG_VIEW_LOOKUPS: true
});

App.Router.map(function() {});

App.ApplicationAdapter = DS.FixtureAdapter.extend({});

App.IndexRoute = Ember.Route.extend({
  model: function() {
    return this.store.find('message');
  }
});

App.IndexController = Ember.ArrayController.extend({
  kittensCount: 0,
  cityOne: 'Ork',
  cityTwo: 'Orsk',
  date: (new Date()).toISOString().slice(0, 10),
  filter: null,
  setupFilter: (function() {
    var filter;
    filter = {
      localization: true,
      plural: false,
      parametrizable: false,
      date: false
    };
    return this.set('filter', Ember.Object.extend(filter).create());
  }).on('init'),
  arrangedContent: (function() {
    var filter, messages;
    filter = this.get('filter');
    messages = this.get('content');
    return messages.filter(function(message) {
      return !filter[message.get('type')];
    });
  }).property('content.@each.type', 'filter.localization', 'filter.plural', 'filter.parametrizable', 'filter.date'),
  actions: {
    changeFilter: function(type) {
      var filter, value;
      filter = this.get('filter');
      value = !filter.get(type);
      return filter.set(type, value);
    },
    addMessage: function(num, title, type, pattern) {
      var message, _results;
      num += 1;
      _results = [];
      while (num -= 1) {
        message = this.store.createRecord('message', {
          title: App.gettext(title) + num,
          type: type,
          text: App.ngettext(pattern, arguments[4], arguments[5])
        });
        _results.push(message.save());
      }
      return _results;
    },
    removeMessage: function(num) {
      var messages;
      messages = this.get('content').slice(0, num);
      messages.invoke('deleteRecord');
      return messages.invoke('save');
    }
  }
});

App.Message = DS.Model.extend({
  title: DS.attr('string'),
  text: DS.attr('string'),
  type: DS.attr('string')
});

App.MessageView = Ember.View.extend({
  classNames: ['message'],
  willAnimateIn: function() {
    return this.$().css('opacity', 0);
  },
  animateIn: function(done) {
    return this.$().fadeTo(500, 1, done);
  },
  animateOut: function(done) {
    return this.$().fadeTo(500, 0, done);
  }
});

App.Message.FIXTURES = [
  {
    "title": 'Localization',
    "text": "Simple message",
    "type": "localization",
    "id": 1
  }, {
    "title": 'Localization',
    "text": "Simple message",
    "type": "localization",
    "id": 2
  }
];

App.gettext = function(value) {
  return value;
};

App.ngettext = function(pattern, value1, value2) {
  return pattern.replace('%1', value1).replace('%2', value2);
};

Ember.Handlebars.helper('gettext', function(value) {
  return value;
});

Ember.Handlebars.helper('getdate', function() {
  var date;
  date = new Date();
  return date.toISOString().slice(0, 10);
});

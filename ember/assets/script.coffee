Ember.LOG_BINDINGS = true;

window.App = Ember.Application.create(
  LOG_TRANSITIONS: true
  LOG_ACTIVE_GENERATION: true
  LOG_VIEW_LOOKUPS: true
)

App.Router.map ->

App.ApplicationAdapter = DS.FixtureAdapter.extend({})

App.IndexRoute = Ember.Route.extend(
  model: ->
    @store.find 'message'
)

App.IndexController = Ember.ArrayController.extend(
  kittensCount: 0
  cityOne: 'Ork'
  cityTwo: 'Orsk'
  date: (new Date()).toISOString().slice(0, 10)
  filter: null

  setupFilter: (()->
    filter =
      localization: true
      plural: false
      parametrizable: false
      date: false

    @set('filter', Ember.Object.extend(filter).create())
  ).on('init')

  arrangedContent: (->
    filter = @get('filter')
    messages = @get('content')
    messages.filter (message) ->
      !filter[message.get('type')]
  ).property('content.@each.type', 'filter.localization', 'filter.plural', 'filter.parametrizable', 'filter.date')

  actions:
    changeFilter: (type)->
      filter = @get('filter')
      value = !filter.get(type)
      filter.set(type, value)

    addMessage: (num, title, type, pattern)->
      num += 1
      while num -= 1
        message = this.store.createRecord('message', {
          title: App.gettext(title) + num
          type: type
          text: App.ngettext(pattern, arguments[4], arguments[5])
        })
        message.save();

    removeMessage: (num) ->
      messages = @get('content').slice(0, num)
      messages.invoke('deleteRecord')
      messages.invoke('save')
)

App.Message = DS.Model.extend(
  title: DS.attr('string')
  text: DS.attr('string')
  type: DS.attr('string')
)

App.MessageView = Ember.View.extend(
  classNames: ['message']

  willAnimateIn: ->
    @$().css 'opacity', 0
  animateIn: (done) ->
    @$().fadeTo 500, 1, done
  animateOut: (done) ->
    @$().fadeTo 500, 0, done
)

App.Message.FIXTURES = [
    {
      "title": 'Localization'
      "text": "Simple message",
      "type": "localization",
      "id": 1
    },
    {
      "title": 'Localization'
      "text": "Simple message",
      "type": "localization",
      "id": 2
    },
  ]

App.gettext = (value)->
  value

App.ngettext = (pattern, value1, value2)->
  pattern.replace('%1', value1).replace('%2', value2)

Ember.Handlebars.helper 'gettext', (value) ->
  value

Ember.Handlebars.helper 'getdate', () ->
  date = new Date()
  date.toISOString().slice(0, 10)



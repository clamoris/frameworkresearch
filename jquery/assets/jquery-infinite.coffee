$( ()->
  constructMessages = (count)->
    i = 0
    messages = []
    types  = ['localization', 'plural', 'parametrizable', 'date']
    template = $("<button class='btn btn-primary' data-target='.bs-example-modal-sm' data-toggle='modal' type='button' />")

    while i < count
      button = template.clone()
      button.text('List item ' + i)
      container = $('<div class="message">').html(button).addClass(types[Math.floor(Math.random()*types.length)])
      messages.push container.prop('outerHTML')
      i += 1

    messages

  $('div.messages').append(constructMessages(25))

  $('div.messages').endlessScroll
    fireOnce: false
    insertAfter: 'div.messages div:last'
    insertBefore: 'body'

    content: (_, __, scrollDirection) ->
      return ' ' if scrollDirection == 'prev' || window.lock
      window.lock = true
      data = constructMessages(1000)
      window.lock = false
      return data.toString().replace(/,/g, '')

  $('.modal').on 'show.bs.modal', (event) ->
    button = $(event.relatedTarget)
    message = button.text()

    modal = $(this)
    modal.find('.modal-body').text message
)

var fs = require('fs');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use('/', express.static(__dirname));
app.use('/react-infinite', express.static(path.join(__dirname, '/react/react-infinite.html')));
app.use('/mithril-infinite', express.static(path.join(__dirname, '/mithril/mithril-infinite.html')));
app.use('/jquery-infinite', express.static(path.join(__dirname, '/jquery/jquery-infinite.html')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/messages', function(req, res) {
  fs.readFile('messages.json', function(err, data) {
    res.setHeader('Content-Type', 'application/json');
    res.send('I am a leaf on the wind. Watch how I soar');
  });
});

app.listen(3000);

console.log('Server started: http://localhost:3000/');

var ModalTrigger = ReactBootstrap.ModalTrigger;
var Modal = ReactBootstrap.Modal;
var Button = ReactBootstrap.Button;

var Message = React.createClass({
    render: function() {
        return (
            <div className={ "message infinite-list-item " + this.props.type } >
                <ModalTrigger bsStyle='primary' bsSize='large' modal={<MyModal _key={ this.props._key } />}>
                    <Button className="message-button">
                        List Item {this.props._key}
                    </Button>
                </ModalTrigger>
            </div>
        );
    }
});

var MyModal = React.createClass({
    render: function() {
        return (
            <Modal {...this.props} bsStyle='primary' title='Some heading, yay!' animation={false}>
                <div className='modal-body'>
                    <h4>{ this.props._key }</h4>
                    <p>List Item {this.props._key}</p>
                </div>
                <div className='modal-footer'>
                    <Button onClick={this.props.onRequestHide}>Close</Button>
                </div>
            </Modal>
        );
    }
});


var InfiniteList = React.createClass({
    getInitialState: function() {
        return {
            elements: this.buildElements(0, 20),
            isInfiniteLoading: false
        }
    },

    buildElements: function(start, end) {
        var elements = [];
        var types  = ['localization', 'plural', 'parametrizable', 'date'];
        for (var i = start; i < end; i++) {
            elements.push(
                    <Message _key={i} key={i} type={ types[Math.floor(Math.random()*types.length)] }/>
            )
        }
        return elements;
    },

    handleInfiniteLoad: function() {
        var that = this;
        this.setState({
            isInfiniteLoading: true
        });
        setTimeout(function() {
            var elemLength = that.state.elements.length,
                newElements = that.buildElements(elemLength, elemLength + 1000);
            that.setState({
                isInfiniteLoading: false,
                elements: that.state.elements.concat(newElements)
            });
        }, 2500);
    },

    elementInfiniteLoad: function() {
        return <div className="infinite-list-item">
            Loading...
        </div>;
    },

    render: function() {
        return (
            <div className="messageBox">
                <div className="messageList">
                    <Infinite className="messages" elementHeight={34}
                        containerHeight={700}
                        infiniteLoadBeginBottomOffset={100}
                        onInfiniteLoad={this.handleInfiniteLoad}
                        loadingSpinnerDelegate={this.elementInfiniteLoad()}
                        isInfiniteLoading={this.state.isInfiniteLoading}
                    >
                        {this.state.elements}
                    </Infinite>
                </div>
            </div>
        );
    }
});

React.render(<InfiniteList/>, document.getElementById('content'));
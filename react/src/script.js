var MessageBox = React.createClass({
    loadMessagesFromServer: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function() {
        return {data: []};
    },
    componentDidMount: function() {
        this.loadMessagesFromServer();
        //setInterval(this.loadMessagesFromServer, this.props.pollInterval);
    },
    sendMessage: function(message) {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            type: 'POST',
            data: message,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    deleteMessage: function(message) {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            type: 'DELETE',
            data: message,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    render: function() {
        return (
            <div className="messageBox">
                <MessageList data={this.state.data} />
                <DestroyButtons onMessageDelete={ this.deleteMessage } />
                <InputFormBox onMessageSubmit={ this.sendMessage }/>
                <div className="clear" />
            </div>
        );
    }
});

var FilterButtons = React.createClass({
    handleChange: function(type, e) {
        var filteredTypes = this.props.filteredTypes;
        filteredTypes[type] = !filteredTypes[type];
        this.props.onUserInput(filteredTypes);
    },
    render: function(){
        return (
            <div className="filterButtons">
                {Object.keys(this.props.filteredTypes).map(function(item) {
                    return (
                    <div className={"ck-button " + item} key={item} >
                        <label>
                            <input type="checkbox"
                                checked={ this.props.filteredTypes[item] }
                                onChange={ this.handleChange.bind(this, item) }>
                                <span>Вкл</span>
                            </input>
                        </label>
                    </div>
                    );
                }, this)}
                <div className="clear" />
            </div>
        )
    }
});

var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var MessageList = React.createClass({
    getInitialState: function() {
        return {
            filteredTypes: {
                localization: false,
                plural: false,
                parametrizable: false,
                date: false
            }
        };
    },
    handleUserInput: function(filteredTypes) {
        this.setState({
            filteredTypes: filteredTypes
        });
    },
    render: function() {
        var self = this;
        var messageNodes = this.props.data.map(function (comment) {
            if (self.state.filteredTypes[comment.type]) {
                return;
            }
            return (
                <Message type={comment.type} title={comment.title} key={comment.id}>
                    {comment.text}
                </Message>
            );
        });
        if (_.compact(messageNodes).length == 0) { messageNodes = <Message title="У вас нет сообщений"/> }

        return (
            <div className="messageList">
                <FilterButtons
                    filteredTypes={this.state.filteredTypes}
                    onUserInput={this.handleUserInput}
                />
                <ReactCSSTransitionGroup className="messages" transitionName="example">
                    { messageNodes }
                </ReactCSSTransitionGroup>
            </div>
        );
    }
});

var Message = React.createClass({
    render: function() {
        return (
            <div className={ "message " + this.props.type } >
                <div className="messageWrapper">
                    <h4 className="messageTitle">
                        {this.props.title}
                    </h4>
                    <div className="messageText">
                        { this.props.children }
                    </div>
                </div>
            </div>
        );
    }
});

var InputFormBox = React.createClass({
    render: function() {
        return (
            <div className="inputFormBox">
                <InputForm title="Локализация" type="localization" onMessageSubmit={ this.props.onMessageSubmit }>
                    Просто сообщение
                </InputForm>
                <InputForm title="Множественное число" type="plural" onMessageSubmit={ this.props.onMessageSubmit }>
                    У нас
                    <input type="text" className='small' />
                    котенка
                </InputForm>
                <InputForm title="Параметризуемое" type="parametrizable" onMessageSubmit={ this.props.onMessageSubmit }>
                    Город
                    <input type="text" className='medium' />
                    мне неизвестен,
                    может быть вы имели ввиду
                    <input type="text" className='medium' />
                </InputForm>
                <InputForm title="Даты" type="date" onMessageSubmit={ this.props.onMessageSubmit }>
                    Сегодняшняя дата - <Clock />
                </InputForm>
            </div>
        );
    }
});


var InputForm = React.createClass({
    handleSubmit: function(e) {
        e.preventDefault();

        // jQuery is used cause else I should create four components with state binding or develop string to input parser.
        // Both cases are too much for the task.

        var $textContainer = $(React.findDOMNode(this.refs.message)).find('.messageText');
        var text = '';

        if ($textContainer.find('input').length > 0) {
            if (_.any($textContainer.find('input').map(function() { return $(this).val().trim() == '' }))) {
                return
            }
            text = $textContainer.find('*').map(function(){
                return $(this).val() || $(this).text()
            });

        } else if (typeof this.props.children == 'object') {
            text = this.props.children.map(function(el) {
                if (typeof el == 'string') {
                    return el
                } else {
                    var textNode = $(React.renderToString(el));
                    return textNode.val() || textNode.text()
                }
            });
        } else {
            text = [this.props.children]
        }

        text = Array.prototype.slice.call(text);
        text = text.join(' ');

        for (var i = parseInt(e.target.value); i > 0; i--) {
            this.props.onMessageSubmit({ title: this.props.title + i, text: text, type: this.props.type });
        }
    },
    render: function() {
        return (
            <div className="inputForm">
                <Message type={this.props.type} title={this.props.title} ref="message">
                    { this.props.children }
                </Message>
                <div className="buttons">
                    <input type="button" value="+1" onClick={ this.handleSubmit }/>
                    <input type="button" value="+10" onClick={ this.handleSubmit } />
                    <input type="button" value="+100" onClick={ this.handleSubmit } />
                </div>
                <div className="clear" />
            </div>
        );
    }
});

var DestroyButtons = React.createClass({
    handleSubmit: function(e) {
        e.preventDefault();
        this.props.onMessageDelete({count: e.target.value});
    },
    render: function() {
        return (
            <div className="destroyButtons">
                <input type="button" value="-1" onClick={ this.handleSubmit }/>
                <input type="button" value="-10" onClick={ this.handleSubmit }/>
                <input type="button" value="-100" onClick={ this.handleSubmit }/>
            </div>
        );
    }
});

var Clock = React.createClass({
    getDefaultProps: function () {
        return {
            date: new Date()
        }
    },
    render: function () {
        return <span>{this.props.date.toISOString().slice(0, 10)}</span>;
    }
});

var Button = React.createClass({
   render: function() {
       return (
           <input type="button" value={ this.props.value }  onSubmit={ this.handleSubmit } />
       )
   }
});

React.render(
    <MessageBox url="messages.json" pollInterval={2000} />,
    document.getElementById('content')
);

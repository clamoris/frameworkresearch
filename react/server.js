/**
 * This file provided by Facebook is for non-commercial testing and evaluation purposes only.
 * Facebook reserves all rights not expressly granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * FACEBOOK BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

var fs = require('fs');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use('/', express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/messages.json', function(req, res) {
  fs.readFile('messages.json', function(err, data) {
    res.setHeader('Content-Type', 'application/json');
    res.send(data);
  });
});

app.post('/messages.json', function(req, res) {
    data = fs.readFileSync('messages.json').toString();
    var messages = JSON.parse(data);
    var message = req.body;
    message.id = Math.max.apply(Math, messages.map(function(m){ return m.id })) + 1;
    messages.push(message);

    fs.writeFileSync('messages.json', JSON.stringify(messages, null, 4));
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Cache-Control', 'no-cache');
    res.send(JSON.stringify(messages));
});

app.delete('/messages.json', function(req, res) {
    fs.readFile('messages.json', function(err, data) {
        var messages = JSON.parse(data);
        var count = Math.abs(req.body.count);

        fs.writeFile('messages.json', JSON.stringify(messages.slice(count), null, 4), function(err) {
            res.setHeader('Content-Type', 'application/json');
            res.setHeader('Cache-Control', 'no-cache');
            res.send(JSON.stringify(messages.slice(count)));
        });
    });
});

app.listen(3000);

console.log('Server started: http://localhost:3000/');

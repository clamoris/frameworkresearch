var MessageBox = React.createClass({displayName: "MessageBox",
    loadMessagesFromServer: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function() {
        return {data: []};
    },
    componentDidMount: function() {
        this.loadMessagesFromServer();
        //setInterval(this.loadMessagesFromServer, this.props.pollInterval);
    },
    sendMessage: function(message) {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            type: 'POST',
            data: message,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    deleteMessage: function(message) {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            type: 'DELETE',
            data: message,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    render: function() {
        return (
            React.createElement("div", {className: "messageBox"}, 
                React.createElement(MessageList, {data: this.state.data}), 
                React.createElement(DestroyButtons, {onMessageDelete:  this.deleteMessage}), 
                React.createElement(InputFormBox, {onMessageSubmit:  this.sendMessage}), 
                React.createElement("div", {className: "clear"})
            )
        );
    }
});

var FilterButtons = React.createClass({displayName: "FilterButtons",
    handleChange: function(type, e) {
        var filteredTypes = this.props.filteredTypes;
        filteredTypes[type] = !filteredTypes[type];
        this.props.onUserInput(filteredTypes);
    },
    render: function(){
        return (
            React.createElement("div", {className: "filterButtons"}, 
                Object.keys(this.props.filteredTypes).map(function(item) {
                    return (
                    React.createElement("div", {className: "ck-button " + item, key: item}, 
                        React.createElement("label", null, 
                            React.createElement("input", {type: "checkbox", 
                                checked:  this.props.filteredTypes[item], 
                                onChange:  this.handleChange.bind(this, item) }, 
                                React.createElement("span", null, "Вкл")
                            )
                        )
                    )
                    );
                }, this), 
                React.createElement("div", {className: "clear"})
            )
        )
    }
});

var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var MessageList = React.createClass({displayName: "MessageList",
    getInitialState: function() {
        return {
            filteredTypes: {
                localization: false,
                plural: false,
                parametrizable: false,
                date: false
            }
        };
    },
    handleUserInput: function(filteredTypes) {
        this.setState({
            filteredTypes: filteredTypes
        });
    },
    render: function() {
        var self = this;
        var messageNodes = this.props.data.map(function (comment) {
            if (self.state.filteredTypes[comment.type]) {
                return;
            }
            return (
                React.createElement(Message, {type: comment.type, title: comment.title, key: comment.id}, 
                    comment.text
                )
            );
        });
        if (_.compact(messageNodes).length == 0) { messageNodes = React.createElement(Message, {title: "У вас нет сообщений"}) }

        return (
            React.createElement("div", {className: "messageList"}, 
                React.createElement(FilterButtons, {
                    filteredTypes: this.state.filteredTypes, 
                    onUserInput: this.handleUserInput}
                ), 
                React.createElement(ReactCSSTransitionGroup, {className: "messages", transitionName: "example"}, 
                     messageNodes 
                )
            )
        );
    }
});

var Message = React.createClass({displayName: "Message",
    render: function() {
        return (
            React.createElement("div", {className:  "message " + this.props.type}, 
                React.createElement("div", {className: "messageWrapper"}, 
                    React.createElement("h4", {className: "messageTitle"}, 
                        this.props.title
                    ), 
                    React.createElement("div", {className: "messageText"}, 
                         this.props.children
                    )
                )
            )
        );
    }
});

var InputFormBox = React.createClass({displayName: "InputFormBox",
    render: function() {
        return (
            React.createElement("div", {className: "inputFormBox"}, 
                React.createElement(InputForm, {title: "Локализация", type: "localization", onMessageSubmit:  this.props.onMessageSubmit}, 
                    "Просто сообщение"
                ), 
                React.createElement(InputForm, {title: "Множественное число", type: "plural", onMessageSubmit:  this.props.onMessageSubmit}, 
                    "У нас", 
                    React.createElement("input", {type: "text", className: "small"}), 
                    "котенка"
                ), 
                React.createElement(InputForm, {title: "Параметризуемое", type: "parametrizable", onMessageSubmit:  this.props.onMessageSubmit}, 
                    "Город", 
                    React.createElement("input", {type: "text", className: "medium"}), 
                    "мне неизвестен," + ' ' +
                    "может быть вы имели ввиду", 
                    React.createElement("input", {type: "text", className: "medium"})
                ), 
                React.createElement(InputForm, {title: "Даты", type: "date", onMessageSubmit:  this.props.onMessageSubmit}, 
                    "Сегодняшняя дата - ", React.createElement(Clock, null)
                )
            )
        );
    }
});


var InputForm = React.createClass({displayName: "InputForm",
    handleSubmit: function(e) {
        e.preventDefault();

        // jQuery is used cause else I should create four components with state binding or develop string to input parser.
        // Both cases are too much for the task.

        var $textContainer = $(React.findDOMNode(this.refs.message)).find('.messageText');
        var text = '';

        if ($textContainer.find('input').length > 0) {
            if (_.any($textContainer.find('input').map(function() { return $(this).val().trim() == '' }))) {
                return
            }
            text = $textContainer.find('*').map(function(){
                return $(this).val() || $(this).text()
            });

        } else if (typeof this.props.children == 'object') {
            text = this.props.children.map(function(el) {
                if (typeof el == 'string') {
                    return el
                } else {
                    var textNode = $(React.renderToString(el));
                    return textNode.val() || textNode.text()
                }
            });
        } else {
            text = [this.props.children]
        }

        text = Array.prototype.slice.call(text);
        text = text.join(' ');

        for (var i = parseInt(e.target.value); i > 0; i--) {
            this.props.onMessageSubmit({ title: this.props.title + i, text: text, type: this.props.type });
        }
    },
    render: function() {
        return (
            React.createElement("div", {className: "inputForm"}, 
                React.createElement(Message, {type: this.props.type, title: this.props.title, ref: "message"}, 
                     this.props.children
                ), 
                React.createElement("div", {className: "buttons"}, 
                    React.createElement("input", {type: "button", value: "+1", onClick:  this.handleSubmit}), 
                    React.createElement("input", {type: "button", value: "+10", onClick:  this.handleSubmit}), 
                    React.createElement("input", {type: "button", value: "+100", onClick:  this.handleSubmit})
                ), 
                React.createElement("div", {className: "clear"})
            )
        );
    }
});

var DestroyButtons = React.createClass({displayName: "DestroyButtons",
    handleSubmit: function(e) {
        e.preventDefault();
        this.props.onMessageDelete({count: e.target.value});
    },
    render: function() {
        return (
            React.createElement("div", {className: "destroyButtons"}, 
                React.createElement("input", {type: "button", value: "-1", onClick:  this.handleSubmit}), 
                React.createElement("input", {type: "button", value: "-10", onClick:  this.handleSubmit}), 
                React.createElement("input", {type: "button", value: "-100", onClick:  this.handleSubmit})
            )
        );
    }
});

var Clock = React.createClass({displayName: "Clock",
    getDefaultProps: function () {
        return {
            date: new Date()
        }
    },
    render: function () {
        return React.createElement("span", null, this.props.date.toISOString().slice(0, 10));
    }
});

var Button = React.createClass({displayName: "Button",
   render: function() {
       return (
           React.createElement("input", {type: "button", value:  this.props.value, onSubmit:  this.handleSubmit})
       )
   }
});

React.render(
    React.createElement(MessageBox, {url: "messages.json", pollInterval: 2000}),
    document.getElementById('content')
);

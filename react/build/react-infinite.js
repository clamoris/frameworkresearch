var ModalTrigger = ReactBootstrap.ModalTrigger;
var Modal = ReactBootstrap.Modal;
var Button = ReactBootstrap.Button;

var Message = React.createClass({displayName: "Message",
    render: function() {
        return (
            React.createElement("div", {className:  "message infinite-list-item " + this.props.type}, 
                React.createElement(ModalTrigger, {bsStyle: "primary", bsSize: "large", modal: React.createElement(MyModal, {_key:  this.props._key})}, 
                    React.createElement(Button, {className: "message-button"}, 
                        "List Item ", this.props._key
                    )
                )
            )
        );
    }
});

var MyModal = React.createClass({displayName: "MyModal",
    render: function() {
        return (
            React.createElement(Modal, React.__spread({},  this.props, {bsStyle: "primary", title: "Some heading, yay!", animation: false}), 
                React.createElement("div", {className: "modal-body"}, 
                    React.createElement("h4", null,  this.props._key), 
                    React.createElement("p", null, "List Item ", this.props._key)
                ), 
                React.createElement("div", {className: "modal-footer"}, 
                    React.createElement(Button, {onClick: this.props.onRequestHide}, "Close")
                )
            )
        );
    }
});


var InfiniteList = React.createClass({displayName: "InfiniteList",
    getInitialState: function() {
        return {
            elements: this.buildElements(0, 20),
            isInfiniteLoading: false
        }
    },

    buildElements: function(start, end) {
        var elements = [];
        var types  = ['localization', 'plural', 'parametrizable', 'date'];
        for (var i = start; i < end; i++) {
            elements.push(
                    React.createElement(Message, {_key: i, key: i, type:  types[Math.floor(Math.random()*types.length)] })
            )
        }
        return elements;
    },

    handleInfiniteLoad: function() {
        var that = this;
        this.setState({
            isInfiniteLoading: true
        });
        setTimeout(function() {
            var elemLength = that.state.elements.length,
                newElements = that.buildElements(elemLength, elemLength + 1000);
            that.setState({
                isInfiniteLoading: false,
                elements: that.state.elements.concat(newElements)
            });
        }, 2500);
    },

    elementInfiniteLoad: function() {
        return React.createElement("div", {className: "infinite-list-item"}, 
            "Loading..."
        );
    },

    render: function() {
        return (
            React.createElement("div", {className: "messageBox"}, 
                React.createElement("div", {className: "messageList"}, 
                    React.createElement(Infinite, {className: "messages", elementHeight: 34, 
                        containerHeight: 700, 
                        infiniteLoadBeginBottomOffset: 100, 
                        onInfiniteLoad: this.handleInfiniteLoad, 
                        loadingSpinnerDelegate: this.elementInfiniteLoad(), 
                        isInfiniteLoading: this.state.isInfiniteLoading
                    }, 
                        this.state.elements
                    )
                )
            )
        );
    }
});

React.render(React.createElement(InfiniteList, null), document.getElementById('content'));
var app;

app = {
  items: [],
  constructItems: function(count) {
    var i, r, types;
    r = [];
    i = 0;
    types = ['localization', 'plural', 'parametrizable', 'date'];
    while (i < count) {
      r.push(m("button.btn.btn-primary." + types[Math.floor(Math.random() * types.length)] + "[data-target='.bs-example-modal-sm'][data-toggle='modal'][type='button'][data-message='" + ('List item ' + i) + "']", 'List item ' + i));
      i += 1;
    }
    return r;
  },
  controller: function() {
    app.items = app.constructItems(45);
    this.ctrlList = new mc.occlusionList.controller(app.items);
  },
  modalable: function(element, isInitialized) {
    if (!isInitialized) {
      return $('.modal').on('show.bs.modal', function(event) {
        var button, message, modal;
        button = $(event.relatedTarget);
        message = button.attr('data-message');
        modal = $(this);
        return modal.find('.modal-body').text(message);
      });
    }
  },
  view: function(ctrl) {
    if (!app.lock && ctrl.ctrlList.scrollTop + 700 > ctrl.ctrlList.contentsHeight) {
      app.lock = true;
      console.log('More items!');
      this.items = this.items.concat(this.constructItems(1000));
      ctrl.ctrlList = new mc.occlusionList.controller(app.items);
      app.lock = false;
    }
    return [
      [
        m(".modal.fade.bs-example-modal-sm[aria-hidden='true'][aria-labelledby='mySmallModalLabel'][role='dialog'][tabindex='-1']", {
          config: app.modalable
        }, m(".modal-dialog.modal-sm", [m(".modal-content", [m(".modal-header", [m("button.close[aria-label='Close'][data-dismiss='modal'][type='button']", [m("span[aria-hidden='true']", "×")]), m("h4.modal-title[id='mySmallModalLabel']", ["Cool header", m("a.anchorjs-link[href='#mySmallModalLabel']", [m("span.anchorjs-icon")])])]), m(".modal-body")])]))
      ], m('.messageBox', m('.messageList', mc.occlusionList.view(ctrl.ctrlList, 700, null, {
        _item: '.message',
        _wrapper: '.messages'
      }, {
        onclick: alert
      })))
    ];
  }
};

m.module(document.body, app);

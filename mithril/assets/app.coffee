app = {}

app.Message = (data) ->
  @title = m.prop(data.title)
  @text = m.prop(data.text)
  @type = m.prop(data.type)
  @

app.MessageList = Array

app.vm = do ->
  vm = {}

  vm.init = ->
    vm.list = new (app.MessageList)
    vm.kittensCount = m.prop(0)
    vm.cityOne = m.prop 'Ork'
    vm.cityTwo = m.prop 'Orsk'
    vm.date = m.prop (new Date()).toISOString().slice(0, 10)
    vm.filter = {
      localization: true
      plural: false
      parametrizable: false
      date: false
    }

    vm.add = (num, options, e)->
      num += 1
      while num -= 1
        vm.list.push new (app.Message)(
          title: gettext(options.title) + num
          type: options.type
          text: ngettext(options.text, options.text_values)
        )

    vm.remove = (num, e)->
      vm.list.splice(0, num)

    vm.filtered = ()->
      vm.list.filter (el)->
        !vm.filter[el.type()]

    vm.toggleFilter = (type)->
      vm.filter[type] = !vm.filter[type]
  vm

app.controller = ->
  app.vm.init()

app.view = ->
  m('.messageBox', [
    m('.messageList', [
      m('.filterButtons', [
        ['localization', 'plural', 'parametrizable', 'date'].map (type)->
          m('.ck-button.' + type, [ m('label', [
            m('input[type=\'checkbox\']',  { onchange: app.vm.toggleFilter.bind(app.vm, type), checked: app.vm.filter[type] })
            m('span', unless app.vm.filter[type] then gettext('On') else gettext('Off'))
          ]) ])
        m('.clear')
      ])
      m('span.messages', [
        app.vm.filtered().map (message) ->
          m(('.message.' + message.type()), { config: fadesIn }, [ m('.messageWrapper', [
            m('h4.messageTitle', message.title())
            m('.messageText', message.text())
          ]) ])
        ( gettext('No messages') if app.vm.filtered().length == 0 )
      ])
    ])
    m('.destroyButtons', [
      [1, 10, 100].map (count)->
        m("input[type=\'button\'][value=\'-#{count}\']", { onclick: app.vm.remove.bind(app.vm, count)} )
    ])
    m('.inputFormBox', [
      m('.inputForm', [
        m('.message.localization', [ m('.messageWrapper', [
          m('h4.messageTitle', gettext('Localization'))
          m('.messageText', gettext('Simple message'))
        ]) ])
        m('.buttons', [
          [1, 10, 100].map (count)->
            m("input[type=\'button\'][value=\'+#{count}\']", { onclick: app.vm.add.bind(app.vm, count, title: 'Localization',  type: 'localization', text: 'Simple message') })
        ])
        m('.clear')
      ])
      m('.inputForm', [
        m('.message.plural', [ m('.messageWrapper', [
          m('h4.messageTitle', gettext('Plural form'))
          m('.messageText', [
            m('span', gettext('We have'))
            m('input.small[type=\'text\']', { onchange: m.withAttr("value", app.vm.kittensCount ), value: app.vm.kittensCount()})
            m('span', gettext('kittens'))
          ])
        ]) ])
        m('.buttons', [
          [1, 10, 100].map (count)->
            m("input[type=\'button\'][value=\'+#{count}\']", { onclick: app.vm.add.bind(app.vm, count, title: 'Plural Form',  type: 'plural', text: 'We have %1 kittens', text_values: [app.vm.kittensCount()]) })
        ])
        m('.clear')
      ])
      m('.inputForm', [
        m('.message.parametrizable', [ m('.messageWrapper', [
          m('h4.messageTitle', gettext('Parametrization'))
          m('.messageText', [
            m('span', gettext 'City' )
            m('input.medium[type=\'text\']', { onchange: m.withAttr("value", app.vm.cityOne ), value: app.vm.cityOne()})
            m('span', gettext 'is unknown, did you mean' )
            m('input.medium[type=\'text\']', { onchange: m.withAttr("value", app.vm.cityTwo ), value: app.vm.cityTwo()})
          ])
        ]) ])
        m('.buttons', [
          [1, 10, 100].map (count)->
            m("input[type=\'button\'][value=\'+#{count}\']", { onclick: app.vm.add.bind(app.vm, count, title: 'Parametrization',  type: 'parametrizable', text: 'City %1 is unknown, did you mean %2?', text_values: [app.vm.cityOne(), app.vm.cityTwo()]) })
        ])
        m('.clear')
      ])
      m('.inputForm', [
        m('.message.date', [ m('.messageWrapper', [
          m('h4.messageTitle', gettext 'Dates')
          m('.messageText', [
            m('span', gettext 'Today is ')
            m('span', app.vm.date())
          ])
        ]) ])
        m('.buttons', [
          [1, 10, 100].map (count)->
            m("input[type=\'button\'][value=\'+#{count}\']", { onclick: app.vm.add.bind(app.vm, count, title: 'Dates',  type: 'date', text: 'Today is %1', text_values: [app.vm.date()]) })
        ])
        m('.clear')
      ])
    ])
    m('.clear')
  ])

gettext = (value)->
  value

ngettext = (pattern, values = [])->
  pattern.replace('%1', values[0]).replace('%2', values[1])

#view helpers

fadesIn = (element, isInitialized, context) ->
  if !isInitialized
    element.style.opacity = 0
    Velocity element, opacity: 1

fadesOut = (callback) ->
  (e) ->
    m.redraw.strategy 'none'
    Velocity e.target, { opacity: 0 }, complete: ->
      m.startComputation()
      callback()
      m.endComputation()

m.module document.getElementById('content'),
  controller: app.controller
  view: app.view

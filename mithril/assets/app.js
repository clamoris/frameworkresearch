var app, fadesIn, fadesOut, gettext, ngettext;

app = {};

app.Message = function(data) {
  this.title = m.prop(data.title);
  this.text = m.prop(data.text);
  this.type = m.prop(data.type);
  return this;
};

app.MessageList = Array;

app.vm = (function() {
  var vm;
  vm = {};
  vm.init = function() {
    vm.list = new app.MessageList;
    vm.kittensCount = m.prop(0);
    vm.cityOne = m.prop('Ork');
    vm.cityTwo = m.prop('Orsk');
    vm.date = m.prop((new Date()).toISOString().slice(0, 10));
    vm.filter = {
      localization: true,
      plural: false,
      parametrizable: false,
      date: false
    };
    vm.add = function(num, options, e) {
      var _results;
      num += 1;
      _results = [];
      while (num -= 1) {
        _results.push(vm.list.push(new app.Message({
          title: gettext(options.title) + num,
          type: options.type,
          text: ngettext(options.text, options.text_values)
        })));
      }
      return _results;
    };
    vm.remove = function(num, e) {
      return vm.list.splice(0, num);
    };
    vm.filtered = function() {
      return vm.list.filter(function(el) {
        return !vm.filter[el.type()];
      });
    };
    return vm.toggleFilter = function(type) {
      return vm.filter[type] = !vm.filter[type];
    };
  };
  return vm;
})();

app.controller = function() {
  return app.vm.init();
};

app.view = function() {
  return m('.messageBox', [
    m('.messageList', [
      m('.filterButtons', [
        ['localization', 'plural', 'parametrizable', 'date'].map(function(type) {
          return m('.ck-button.' + type, [
            m('label', [
              m('input[type=\'checkbox\']', {
                onchange: app.vm.toggleFilter.bind(app.vm, type),
                checked: app.vm.filter[type]
              }), m('span', !app.vm.filter[type] ? gettext('On') : gettext('Off'))
            ])
          ]);
        }), m('.clear')
      ]), m('span.messages', [
        app.vm.filtered().map(function(message) {
          return m('.message.' + message.type(), {
            config: fadesIn
          }, [m('.messageWrapper', [m('h4.messageTitle', message.title()), m('.messageText', message.text())])]);
        }), (app.vm.filtered().length === 0 ? gettext('No messages') : void 0)
      ])
    ]), m('.destroyButtons', [
      [1, 10, 100].map(function(count) {
        return m("input[type=\'button\'][value=\'-" + count + "\']", {
          onclick: app.vm.remove.bind(app.vm, count)
        });
      })
    ]), m('.inputFormBox', [
      m('.inputForm', [
        m('.message.localization', [m('.messageWrapper', [m('h4.messageTitle', gettext('Localization')), m('.messageText', gettext('Simple message'))])]), m('.buttons', [
          [1, 10, 100].map(function(count) {
            return m("input[type=\'button\'][value=\'+" + count + "\']", {
              onclick: app.vm.add.bind(app.vm, count, {
                title: 'Localization',
                type: 'localization',
                text: 'Simple message'
              })
            });
          })
        ]), m('.clear')
      ]), m('.inputForm', [
        m('.message.plural', [
          m('.messageWrapper', [
            m('h4.messageTitle', gettext('Plural form')), m('.messageText', [
              m('span', gettext('We have')), m('input.small[type=\'text\']', {
                onchange: m.withAttr("value", app.vm.kittensCount),
                value: app.vm.kittensCount()
              }), m('span', gettext('kittens'))
            ])
          ])
        ]), m('.buttons', [
          [1, 10, 100].map(function(count) {
            return m("input[type=\'button\'][value=\'+" + count + "\']", {
              onclick: app.vm.add.bind(app.vm, count, {
                title: 'Plural Form',
                type: 'plural',
                text: 'We have %1 kittens',
                text_values: [app.vm.kittensCount()]
              })
            });
          })
        ]), m('.clear')
      ]), m('.inputForm', [
        m('.message.parametrizable', [
          m('.messageWrapper', [
            m('h4.messageTitle', gettext('Parametrization')), m('.messageText', [
              m('span', gettext('City')), m('input.medium[type=\'text\']', {
                onchange: m.withAttr("value", app.vm.cityOne),
                value: app.vm.cityOne()
              }), m('span', gettext('is unknown, did you mean')), m('input.medium[type=\'text\']', {
                onchange: m.withAttr("value", app.vm.cityTwo),
                value: app.vm.cityTwo()
              })
            ])
          ])
        ]), m('.buttons', [
          [1, 10, 100].map(function(count) {
            return m("input[type=\'button\'][value=\'+" + count + "\']", {
              onclick: app.vm.add.bind(app.vm, count, {
                title: 'Parametrization',
                type: 'parametrizable',
                text: 'City %1 is unknown, did you mean %2?',
                text_values: [app.vm.cityOne(), app.vm.cityTwo()]
              })
            });
          })
        ]), m('.clear')
      ]), m('.inputForm', [
        m('.message.date', [m('.messageWrapper', [m('h4.messageTitle', gettext('Dates')), m('.messageText', [m('span', gettext('Today is ')), m('span', app.vm.date())])])]), m('.buttons', [
          [1, 10, 100].map(function(count) {
            return m("input[type=\'button\'][value=\'+" + count + "\']", {
              onclick: app.vm.add.bind(app.vm, count, {
                title: 'Dates',
                type: 'date',
                text: 'Today is %1',
                text_values: [app.vm.date()]
              })
            });
          })
        ]), m('.clear')
      ])
    ]), m('.clear')
  ]);
};

gettext = function(value) {
  return value;
};

ngettext = function(pattern, values) {
  if (values == null) {
    values = [];
  }
  return pattern.replace('%1', values[0]).replace('%2', values[1]);
};

fadesIn = function(element, isInitialized, context) {
  if (!isInitialized) {
    element.style.opacity = 0;
    return Velocity(element, {
      opacity: 1
    });
  }
};

fadesOut = function(callback) {
  return function(e) {
    m.redraw.strategy('none');
    return Velocity(e.target, {
      opacity: 0
    }, {
      complete: function() {
        m.startComputation();
        callback();
        return m.endComputation();
      }
    });
  };
};

m.module(document.getElementById('content'), {
  controller: app.controller,
  view: app.view
});
